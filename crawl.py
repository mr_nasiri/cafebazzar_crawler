import json
import requests
import os

from abc import ABC, abstractmethod
from config import HEADER, CATEGORIES, BASE_LINK_API, links_payload, BASE_EXPAND_LINK_API,\
    expand_links_payload, BASE_DETAIL_API, details_payload, BASE_LINK_SC, color
from parser import Parser


class CrawlerBase(ABC):

    def __init__(self):
        self.parser = Parser()

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def store(self, data, filename=None):
        pass

    @staticmethod
    def post(url, asked_data, payload, type_post):
        """
        :param url: api link
        :param asked_data: can be app path or category name
        :param payload: set payload for request to api
        :param type_post: can be link , expand_links or data Which determines which api_link to be used.
        """

        if type_post == 'link':
            payload['singleRequest']['getPageV2Request']['path'] = asked_data
        elif type_post == 'expand_link':
            payload['singleRequest']['getPageBodyRequest']['path'] = asked_data
        else:
            payload['singleRequest']['appDetailsV2Request']['packageName'] = asked_data

        try:
            response = requests.post(url, data=json.dumps(payload), headers=HEADER)
        except requests.HTTPError:
            return None

        if response.status_code == 200:
            return response

        return None


class LinkCrawler(CrawlerBase):
    """
    in link crawler class , we request to api by send categories path and get all apps link (path)
    then parse and save it in apps_paths file
    """

    def __init__(self, api_link=BASE_LINK_API, categories=CATEGORIES):
        self.api_link = api_link
        self.categories = categories
        super().__init__()

    def crawl_expand_links(self, expand_links):
        expand_links_apps = list()
        for expand_link in expand_links:
            response = self.post(self.api_link, expand_link, links_payload, 'link')
            adv_links = self.parser.link_parser(json.loads(response.content))
            expand_links_apps.extend(adv_links['app_links'])

            i = 24
            while True:
                expand_links_payload['singleRequest']['getPageBodyRequest']['offset'] = i
                response = self.post(BASE_EXPAND_LINK_API, expand_link, expand_links_payload, 'expand_link')
                adv_links = self.parser.link_parser(json.loads(response.content))
                expand_links_apps.extend(adv_links['app_links'])
                if len(adv_links['app_links']) < 24:
                    break
                i += 24
        return expand_links_apps

    def start_crawl_category(self, category):
        category_apps_links = list()
        response = self.post(self.api_link, category, links_payload, 'link')
        adv_links = self.parser.link_parser(json.loads(response.content))
        category_apps_links.extend(adv_links['app_links'])
        expand_links_apps = self.crawl_expand_links(adv_links['expand_links'])
        category_apps_links.extend(expand_links_apps)
        return category_apps_links

    def start(self):
        apps_links = list()
        for category in self.categories:
            adv_links = self.start_crawl_category(category)
            adv_links = set(adv_links)
            print(f"{color['violet']}{category}: {color['cyan']}{len(adv_links)}")
            apps_links.extend(adv_links)
        apps_links = set(apps_links)
        print(f"{color['green']}task is done. total apps: {len(apps_links)}")
        self.store([{"path": link} for link in apps_links], 'apps_paths')

    def store(self, data, filename=None):
        with open(f'fixtures/{filename}.json', 'w') as f:
            f.write(json.dumps(data))


class DataCrawler(CrawlerBase):
    """
    in data crawler class , we load apps path from apps_paths file
     and request to api by send apps links and get all information about apps
     then parse and save them.
    """

    def __init__(self, api_link=BASE_DETAIL_API):
        self.api_link = api_link
        self.apps_links = self.__load_apps_links()
        super().__init__()

    @staticmethod
    def __load_apps_links():
        with open("fixtures/apps_paths.json", 'r') as f:
            return json.loads(f.read())

    def start(self):
        for link in self.apps_links:
            response = self.post(self.api_link, link['path'], details_payload, 'app_data')
            data = self.parser.data_parser(json.loads(response.content))
            self.store(data, data['package_name'])
        print(f"{color['green']}task is done.")

    def store(self, data, filename=None):
        with open(f"fixtures/adv/{filename}.json", 'w') as f:
            f.write(json.dumps(data))
        print(f"{color['end_color']}{data['package_name']}{color['green']} saved.")


class ImageDownloader(CrawlerBase):
    """
    in image downloader class we send get request to api for download all apps screenshots
    and save them in folders with apps package names.
    """

    def __init__(self, link=BASE_LINK_SC):
        self.link = link
        self.apps = self.__load_apps()
        super().__init__()

    @staticmethod
    def __load_apps():
        apps = list()
        with open("fixtures/apps_paths.json", 'r') as f:
            apps_paths = json.loads(f.read())

        for app_path in apps_paths:
            with open(f"fixtures/adv/{app_path['path']}.json", 'r') as file:
                apps.append(json.loads(file.read()))
        return apps

    @staticmethod
    def get(link):
        try:
            response = requests.get(link, stream=True)
        except requests.HTTPError:
            return None

        if response.status_code == 200:
            return response

        return None

    def start(self):
        for app in self.apps:
            counter = 1
            os.mkdir(f"fixtures/images/{app['package_name']}")
            for image in app['screenshots_urls'].values():
                response = self.get(self.link + image)
                self.store(response, counter, app['package_name'])
                counter += 1
            print(f"{color['end_color']}{app['package_name']} {color['green']}({counter-1}) images saved.")

        print(f"{color['green']}task is done.")

    def store(self, data, img_number, dir_name):
        with open(f"fixtures/images/{dir_name}/{img_number}.jpg", 'wb') as f:
            for chunk in data.iter_content(chunk_size=1024):
                f.write(chunk)
