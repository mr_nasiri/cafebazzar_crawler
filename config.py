# api for get apps links
BASE_LINK_API = 'https://api.cafebazaar.ir/rest-v1/process/GetPageV2Request'

# api for get apps details
BASE_DETAIL_API = 'https://api.cafebazaar.ir/rest-v1/process/AppDetailsV2Request'

# api for get expand apps links
BASE_EXPAND_LINK_API = 'https://api.cafebazaar.ir/rest-v1/process/GetPageBodyRequest'

# using for complete apps links
BASE_LINK = 'https://cafebazaar.ir/app/'

# using for download apps screenshots
BASE_LINK_SC = 'https://s.cafebazaar.ir/1/'

HEADER = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                  '(KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
}

# payload for get apps links
links_payload = {
    "properties": {
        "clientID": "j3uzn4m30sfi6scoxltrmniylkvxm0yh",
        "deviceID": "j3uzn4m30sfi6scoxltrmniylkvxm0yh",
        "clientVersion": "web",
        "language": 2
    },
    "singleRequest": {
        "getPageV2Request": {"path": "category_weather"}
    }
}

expand_links_payload = {
    "properties": {
        "clientID": "j3uzn4m30sfi6scoxltrmniylkvxm0yh",
        "deviceID": "j3uzn4m30sfi6scoxltrmniylkvxm0yh",
        "clientVersion": "web",
        "language": 2
    },
    "singleRequest": {
        "getPageBodyRequest": {"path": "list~app~automatic~APP_BEST~weather", "offset": 24}}
}

# payload for get apps details
details_payload = {
    "properties": {
        "clientID": "j3uzn4m30sfi6scoxltrmniylkvxm0yh",
        "deviceID": "j3uzn4m30sfi6scoxltrmniylkvxm0yh",
        "clientVersion": "web",
        "language": 2,
    },
    "singleRequest": {
        "appDetailsV2Request": {"packageName": "com.insta.savesmx"}
    }
}

CATEGORIES = [
    'home-app',
    'category_weather',
    # 'category_food-drink',
    # 'category_education',
    # 'category_tools',
    # 'category_finance',
    # 'category_medical',
    # 'category_health-fitness',
    # 'category_shopping',
    # 'category_maps-navigation',
    # 'category_lifestyle',
    # 'category_entertainment',
    # 'category_travel-local',
    # 'category_social',
    # 'category_personalization',
    # 'category_photography',
    # 'category_books-reference',
    # 'category_kids-apps',
    # 'category_religious',
    # 'category_music-audio',
    # 'category_sports',
    # 'home-game',
    # 'category_strategy',
    # 'category_action',
    # 'category_arcade',
    # 'category_casual',
    # 'category_racing',
    # 'category_simulation',
    # 'category_word-trivia',
    # 'category_kids-games',
    # 'category_puzzle',
    # 'category_sports-game',
]

color = {
    "violet": "\033[95m",
    "green": "\033[92m",
    "cyan": "\033[96m",
    "end_color": "\033[0m"
}
