import sys
from crawl import LinkCrawler, DataCrawler, ImageDownloader

if __name__ == '__main__':
    switch = sys.argv[1]
    if switch == 'find_links':
        crawler = LinkCrawler()
        crawler.start()
    elif switch == 'apps_data':
        crawler = DataCrawler()
        crawler.start()
    elif switch == 'download_images':
        crawler = ImageDownloader()
        crawler.start()
