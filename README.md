# Cafe Bazaar Crawler
This script is a crawler which extract all apps and information about them from [Cafe Bazzar](https://cafebazaar.ir/)

## This project is supposed to gather below information
- app name
- app icon link
- app version
- app description
- app package name
- app link in CafeBazzar
- app homepage link
- purchase in app
- app last update
- minimum android version require
- app author
- app category name
- app price
- app rate
- comment count
- install count
- app size
- permissions that app need
- app screenshots

## Requirements
- Python 3.6+

## Quick Start
- Create a directory and name it "CafeBazzar Crawler"
- Open a terminal in this directory
- Create a virtualenv:

    ```shell script
    virtualenv -p python3 Venv
    ```
- Activate virtualenv:

    ```shell script
    sourse Venv/bin/activate
    ```

- Clone this project:

    ```shell script
    git clone https://gitlab.com/mr_nasiri/cafebazzar_crawler.git src
    ```
- Change directory to "src":

    ```shell script
    cd src
    ```
- Install require packages:

    ```shell script
    pip install -r requirements.txt
    ```
- Run main.py and enjoy...

    - Run main.py with 'find_links' parameter to find apps links:

        ```shell script
        python main.py find_links
        ```
    - Run main.py with 'extract_links' parameter to extract information of apps:

        ```shell script
        python main.py extract_links
        ```
    - Run main.py with 'download_images' parameter to download screenshots of apps:

        ```shell script
        python main.py download_images
        ```
## Bulit with:
- [Requests](https://pypi.org/project/requests/) -  Used for make HTTP requests to get data from api
