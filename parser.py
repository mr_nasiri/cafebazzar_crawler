from config import BASE_LINK


class Parser:

    @staticmethod
    def link_parser(data):

        app_links = list()
        expand_links = list()

        get_page_reply = data['singleReply'].get('getPageV2Reply')
        if get_page_reply:
            rows = data['singleReply']['getPageV2Reply']['page']['pageBodyInfo']['pageBody']['rows']
        else:
            rows = data['singleReply']['getPageBodyReply']['pageBody']['rows']

        for row in rows:
            simple_app_list = row.get('simpleAppList')
            if simple_app_list:
                simple_app = simple_app_list
            else:
                simple_app = row.get('simpleAppItem')
            if simple_app:
                apps = simple_app.get('apps')
                if apps:
                    for app in apps:
                        app_links.append(app['info']['packageName'])
                    if simple_app['expandInfo']['show']:
                        expand_links.append(simple_app['expandInfo']['vitrinExpandInfo']['path'])
                else:
                    app_links.append(simple_app['info']['packageName'])

        links = {
            'app_links': app_links,
            'expand_links': expand_links
        }
        return links

    @staticmethod
    def data_parser(data):
        data = data['singleReply']['appDetailsV2Reply']

        app_data = {
            'app_name': data['meta']['name'],
            'icon_url': data['media']['iconUrl'],
            'version': data['package']['versionName'],
            'description': data['meta']['shortDescription'],
            'package_name': data['package']['name'],
            'app_url': BASE_LINK + data['package']['name'],
            'app_homepage_url': data['meta']['homepageUrl'],
            'purchase_in_app': data['meta']['hasInAppPurchase'],
            'last_update': data['package']['lastUpdated'],
            'min_android_ver': data['package']['minimumSDKVersion'],
            'app_author': data['meta']['author']['name'],
            'app_category_name': data['meta']['category']['name'],
            'app_price': data['meta']['payment']['price'],
            'app_rate': data['meta']['reviewInfo']['averageRate'],
            'comment_count': data['meta']['reviewInfo']['reviewCount'],
            'app_install_count': data['meta']['installCount']['range'],
            'app_size': data['package']['verboseSize'] + ' ' + data['package']['verboseSizeLabel'],
            'permissions': {count: value for count, value in enumerate(data['package']['permissions'])},
            'screenshots_urls': {count+1: value['fullSize'] for count, value in enumerate(data['media']['screenshots'])}
        }

        return app_data
